// New practice in importing components;
//import { useContext } from 'react';
import { Row, Col } from 'react-bootstrap';


export default function Footer() {

	return (
		<Row id="content1">

		<Col lg="3" xs="6" className='mb-4 mb-md-0 p-5'>
				<Row className='my-auto'>
					<Col sm="3">
						<span><img
		              src={require('../img/content/package.png')}
		              width="100%" className='mb-3'
		              alt="D&M AutoParts"
		            /></span>
					</Col>
					<Col sm="9">
						<h5>We Deliver Nationwide</h5>
						<p>For orders P3,000</p>
					</Col>
				</Row>

			</Col>

			<Col lg="3" xs="6" className='mb-4 mb-md-0 p-5'>
				<Row className='my-auto'>
					<Col sm="3">
						<span><img
		              src={require('../img/content/free-delivery.png')}
		              width="100%" className='mb-3'
		              alt="D&M AutoParts"
		            /></span>
					</Col>
					<Col sm="9">
						<h5>Free Shipping</h5>
						<p>For orders P3,000</p>
					</Col>
				</Row>
			</Col>

			<Col lg="3" xs="6" className='mb-4 mb-md-0 p-5'>
				<Row className='my-auto'>
					<Col sm="3">
						<span><img
		              src={require('../img/content/24-hours.png')}
		              width="100%" className='mb-3'
		              alt="D&M AutoParts"
		            /></span>
					</Col>
					<Col sm="9">
						<h5>Support 24/7</h5>
						<p>Call us anytime</p>
					</Col>
				</Row>
			</Col>

			<Col lg="3" xs="6" className='mb-4 mb-md-0 p-5'>
				<Row className='my-auto'>
					<Col sm="3">
						<span><img
		              src={require('../img/content/credit-card.png')}
		              width="100%" className='mb-3'
		              alt="D&M AutoParts"
		            /></span>
					</Col>
					<Col sm="9">
						<h5>100% Safety</h5>
						<p>Only Secure Payments</p>
					</Col>
				</Row>

			</Col>

		</Row>
	)
}
