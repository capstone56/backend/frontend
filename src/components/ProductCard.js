import { Col, Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../App.css';

//import Resources from './Resources'

export default function ProductCard({productProp}){

    // checks to see if the data wass successfully passed
    //console.log(productsData);

    // Every component receives in a form of an object
    //console.log(console.log[0]);

    //console.log({productProp});

    const { name, price, _id } = productProp;
  


  return(

    <Col xs={12}md={4} lg={3} className="my-3" >
            <Card id="myCard">
                <Card.Body >

                    <Button variant='flat' as={ Link } to={`/products/${_id}`}><img
                        className="d-block w-100"
                        src={require('../img/fake-img.png')}
                        alt="First slide"
                    /></Button>

                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    
                </Card.Body></Card>
        </Col>


  )
}