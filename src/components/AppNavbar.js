// Old practice in importing components
// import Navbar from 'react-bootstrap/NavBar';
// import Nav from 'react-bootstrap/Nav';

// New practice in importing components;
import { useContext } from 'react';
import { Navbar, Nav, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';


export default function AppNavbar() {

	// useContext hook
	const { user } = useContext(UserContext);

	// getItem retrieves the data stored in the localStorage
	// const [user, setUser] = useState(localStorage.getItem("email"))
	console.log(user);

	return (

		<Navbar expand="lg" id="myNavbar">
		    <Container fluid>
		    	<Navbar.Brand as={Link} to="/">
			      	<img
		              src={require('../img/logo/logo.png')}
		              //width="30"
		              height="30"
		              alt="D&M AutoParts"
		            />
		        </Navbar.Brand>
		        <Navbar.Toggle aria-controls="navbarScroll" />
		        <Navbar.Collapse id="navbarScroll">
			      	{/*<form className="d-flex">
			      		<Form.Control
			      			type="search for.."
			      			placeholder="Search"
			      			className="me-2"/>
			      		<Button variant="btn btn-primary">Search</Button>
			      	</form>*/}
			    <Nav className="ms-auto">

			    {
			    	(user.id === null) ?
			    	<>
			    		<Nav.Link as={Link} to="/products" >shop now!</Nav.Link>
			    		<Nav.Link as={Link} to="/register" >sign up</Nav.Link>
			    		<Nav.Link as={Link} to="/login" >log in</Nav.Link>
			    	</>
			    	: (user.isAdmin) ?
			    	<>
			    		<Nav.Link as={Link} to="/admin" >product management</Nav.Link>
			          	<Nav.Link as={Link} to="/allorders" >order management</Nav.Link>
			          	<Nav.Link as={Link} to="/logout" >logout</Nav.Link>
			    	</>
			    	: <>
			    	<Nav.Link as={Link} to="/products" >shop now!</Nav.Link>
			    	<Nav.Link as={Link} to="/myorders" >orders</Nav.Link>
			    	<Nav.Link as={Link} to="/logout" >logout</Nav.Link>
			    	</>
			    }
		          
		        </Nav>
		      </Navbar.Collapse>
		    </Container>
		  </Navbar>
	)
};