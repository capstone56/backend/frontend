import Carousel from 'react-bootstrap/Carousel';

export default function DarkVariantExample() {
  return (
    <Carousel variant="light" className="banner">
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={require('../img/banner/1.png')}
          alt="First slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={require('../img/banner/2.png')}
          alt="Second slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={require('../img/banner/3.png')}
          alt="Third slide"
        />
      </Carousel.Item>
    </Carousel>
  );
}