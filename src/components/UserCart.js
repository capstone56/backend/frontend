import { useState, useContext } from 'react';
import {Card, Table, Modal, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function ProductCard({prop}) {
  
  const { user } = useContext(UserContext);

  const {_id, name, category, description, price, quantity} = prop;

  const [setName, setsetName] = useState(name);
  const [setCategory, setsetCategory] = useState(category);
  const [setDescription, setsetDescription] = useState(description);
  const [setPrice, setsetPrice] = useState(price);
  const [setQuantity, setsetQuantity] = useState(quantity);
  const [setIsActive, setsetIsActive] = useState(isActive);

  const showModal = () => {

    return setIsOpen(true);

  };

  const hideModal = () => {

    return setIsOpen(false);
  };

  const showModal2 = () => {

    return setIsOpen2(true);
  };

  const hideModal2 = () => {

    return setIsOpen2(false);
  };

  const handleEditProduct = (e) => {

    e.preventDefault();

    return fetch(`${process.env.REACT_APP_API_URL}products/:id`, {

      method: 'PUT',
      
      headers: {
        'Content-Type':'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },

      body: JSON.stringify({
        id: _id,
        name: setName,
        category: setCategory,
        description: setDescription,
        price: setPrice,
        quantity: setQuantity,
        isActive: setIsActive
      })
    })
    .then(res => res.json())
    .then(data => {

      if (data.result === true) {

        hideModal();

        return Swal.fire({
          title: 'Successfully Save Changes!',
          icon: 'success',
        });
      };

    });
  };

  const handleAddToCart = (e) => {

    e.preventDefault();

    return fetch(`${process.env.REACT_APP_API_URL}products/add-to-cart`, {
      
      method: 'POST',

      headers: {
        'Content-Type':'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },

      body: JSON.stringify({
        sku: sku,
        quantity: quantity,
      })
    })
    .then(res => res.json())
    .then(data => {
      if (data.result === true) {

        hideModal2();

        return Swal.fire({
          title: 'Added To Cart Successfully!',
          icon: 'success',
        });
      };
    })
  }

	return (
    (user.id === null)
    ? <>
      <div className="col-lg-4">
        <Card className="container">
          <Card.Body>
            <Card.Title>{brand}</Card.Title>
            <Card.Subtitle className="mb-3">{`${model} | ${color}`}</Card.Subtitle>
            <Card.Text> Price: PHP {price}.00</Card.Text>
            <button type="button" className="btn btn-outline-danger rounded-pill w-100" onClick={showModal}>View Details</button>
            <Modal show={isOpen} onHide={hideModal}>
              <Modal.Header>
                <Modal.Title><h4>{brand} ({`${model} | ${color}`})</h4></Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Table striped >
                  <tbody >
                    <tr>
                      <td>Details:</td>
                      <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla in congue erat. In hac habitasse platea dictumst. Cras at enim mauris. Nulla rhoncus tempor ultrices.</td>
                    </tr>
                    <tr>
                      <td>Price:</td>
                      <td>PHP {price}.00</td>
                    </tr>
                    <tr>
                      <td>Style:</td>
                      <td>{style}</td>
                    </tr>
                    <tr>
                      <td>SKU:</td>
                      <td>{sku}</td>
                    </tr>
                  </tbody>
                </Table>
              </Modal.Body>
              <Modal.Footer>
                  <Button variant="outline-danger rounded-pill" onClick={hideModal}>Close</Button>
              </Modal.Footer>
            </Modal>
          </Card.Body>
        </Card>
      </div>
    </>
    : (user.isAdmin)
      ? <>
        <section className="container overflow-auto">
          <div className="row justify-content-center">
            <div className="col-lg-10">
              <Card className="container">
                <Card.Body>
                  <Card.Title>{`Brand: ${brand}`}</Card.Title>
                  <Card.Subtitle className="mb-3">{`Product ID: ${_id}`}</Card.Subtitle>
                    <Table striped >
                      <tbody >
                        <tr>
                          <td>Style:</td>
                          <td>{style}</td>
                        </tr>
                        <tr>
                          <td>Model:</td>
                          <td>{model}</td>
                        </tr>
                        <tr>
                          <td>Size And Color:</td>
                          <td>{`${size} | ${color}`}</td>
                        </tr>
                        <tr>
                          <td>SKU:</td>
                          <td>{sku}</td>
                        </tr>
                        <tr>
                          <td>Quantity and Price:</td>
                          <td>{`${stocks} | ${price}`}</td>
                        </tr>
                        <tr>
                          <td>Active:</td>
                          <td>{(isActive) ? 'Yes' : 'No'}</td>
                        </tr>
                      </tbody>
                    </Table>
                  <button type="button" className="btn btn-outline-danger rounded-pill" onClick={showModal}>Edit</button>
                  <Modal show={isOpen} onHide={hideModal}>
                    <Form onSubmit={e => handleEditProduct(e)}>
                    <Modal.Header>
                      <Modal.Title><h4>Edit Product</h4></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <Form.Group>
                          <Form.Control 
                              className="mb-2"
                              type="text"
                              value={setBrand}
                              onChange={e => setsetBrand(e.target.value)}
                              placeholder="* Brand (e.g. SMK, AVG, LS2)"/>
                          <Form.Select aria-label="Default select example" className="mb-2" value={setStyle} onChange={e => setsetStyle(e.target.value)}>
                              <option>Select Style</option>
                              <option value="Full-face">Full Face</option>
                              <option value="Half-face">Half Face</option>
                              <option value="Modular">Modular</option>
                              <option value="Off road">Off Road</option>
                          </Form.Select>
                          <Form.Control 
                              className="mb-2"
                              type="text"
                              value={setModel}
                              onChange={e => setsetModel(e.target.value)}
                              placeholder="* Model Name" />
                          <Form.Select aria-label="Default select example" className="mb-2" value={setSize} onChange={e => setsetSize(e.target.value)}>
                              <option>Select Size</option>
                              <option value="XS">Extra Small</option>
                              <option value="SM">Small</option>
                              <option value="MD">Medium</option>
                              <option value="LG">Large</option>
                              <option value="XL">Extra Large</option>
                          </Form.Select>
                          <Form.Control 
                              className="mb-2"
                              type="text"
                              value={setColor}
                              onChange={e => setsetColor(e.target.value)}
                              placeholder="* Color" />
                          <Form.Control 
                              className="mb-2"
                              type="text"
                              value={setSku}
                              onChange={e => setsetSku(e.target.value)}
                              placeholder="* SKU (e.g. EXHE-STEXS000)" />
                          <Form.Control 
                              className="mb-2"
                              type="text"
                              value={setPrice}
                              onChange={e => setsetPrice(e.target.value)}
                              placeholder="* Price" />
                          <Form.Control 
                              className="mb-2"
                              type="text"
                              value={setStocks}
                              onChange={e => setsetStocks(e.target.value)}
                              placeholder="* Stocks" />
                          <Form.Select aria-label="Default select example" className="mb-2" value={setIsActive} onChange={e => setsetIsActive(e.target.value)}>
                              <option>Yes/No</option>
                              <option value="true">Yes</option>
                              <option value="false">No</option>
                          </Form.Select>
                      </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="outline-danger rounded-pill" type="submit">Save</Button>
                        <Button variant="outline-danger rounded-pill" onClick={hideModal}>Cancel</Button>
                    </Modal.Footer>
                    </Form>
                  </Modal>
                </Card.Body>
              </Card>
            </div>
          </div>
        </section>
      </>
      : <>
        <div className="col-lg-4">
          <Card className="container">
            <Card.Body>
              <Card.Title>{brand}</Card.Title>
              <Card.Subtitle className="mb-3">{`${model} | ${color}`}</Card.Subtitle>
              <Card.Text> Price: PHP {price}.00</Card.Text>
              <button type="button" className="btn btn-outline-danger rounded-pill w-100 mb-1" onClick={showModal2}>Add To Cart</button>
              <button type="button" className="btn btn-outline-danger rounded-pill w-100" onClick={showModal}>View Details</button>
              <Modal show={isOpen2} onHide={hideModal2}>
                <Form onSubmit={e => handleAddToCart(e)}>
                  <Modal.Header>
                    <Modal.Title><h4>{brand} ({`${model} | ${color} - PHP ${price}.00`}) </h4></Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <Form.Group>
                      <Form.Control
                        className="mb-2" 
                        type="text"
                        value={sku} 
                        readOnly/>
                      <Form.Control
                        type="number"
                        value={qty}
                        onChange={e => setQty(e.target.value)} />
                    </Form.Group>  
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="outline-danger rounded-pill" type="submit">Add</Button>
                  </Modal.Footer>
                </Form>
              </Modal>
              <Modal show={isOpen} onHide={hideModal}>
                <Modal.Header>
                  <Modal.Title><h4>{brand} ({`${model} | ${color}`})</h4></Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <Table striped >
                    <tbody >
                      <tr>
                        <td>Details:</td>
                        <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla in congue erat. In hac habitasse platea dictumst. Cras at enim mauris. Nulla rhoncus tempor ultrices.</td>
                      </tr>
                      <tr>
                        <td>Price:</td>
                        <td>PHP {price}.00</td>
                      </tr>
                      <tr>
                        <td>Style:</td>
                        <td>{style}</td>
                      </tr>
                      <tr>
                        <td>SKU:</td>
                        <td>{sku}</td>
                      </tr>
                    </tbody>
                  </Table>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-danger rounded-pill" onClick={hideModal}>Close</Button>
                </Modal.Footer>
              </Modal>
            </Card.Body>
          </Card>
        </div>
      </>
	);
};