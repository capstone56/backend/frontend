// New practice in importing components;
//import { useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';


export default function Footer() {

	return (
		<Row id="footer">
			<Col lg="4" sm="12" className='mb-4 mb-md-0 p-5'>
				<span><img
		              src={require('./logo.png')}
		              width="100%" className='mb-3'
		              alt="D&M AutoParts"
		            /></span>
				<p>D&M AutoParts was established in 2022 to cater automotive marketplace in the Philippines. D&M AutoParts provides a wide selection of merchandise, with the best possible prices on quality top brand-name products. We at D&M AutoParts understand you have a choice, and we would like to thank you for choosing us.</p>
			</Col>

			<Col lg="4" sm="12" className='mb-4 mb-md-0 p-5'>
				<h4>Newsletter</h4>
				<p>Enter your email address below to subscribe to our newsletter and keep up to date with discounts and special offers.</p>

				<Form>
					<Form.Group className="mb-3" controlId="userEmail">
						<Row><Col sm="8">
							<Form.Control type="email" placeholder="Enter email"/>
						</Col>
						<Col sm="2">
							<Button variant="primary" type="submit" id="submitBtn">Subscribe</Button>
						</Col></Row>
					</Form.Group>

					
				</Form>
			</Col>

			<Col lg="4" sm="12" className='mb-4 mb-md-0 p-5'>
				<h4>Contact Us</h4>
				<p>Hi, we are always open for cooperation and suggestions, contact us in one of the ways below:</p>
				<Row>
					<Col lg="6" sm="6">
						<h6>PHONE NUMBER</h6>
						<p>+63 918 699 3096</p>
					</Col>
					<Col lg="6" sm="6">
						<h6>EMAIL ADDRESS</h6>
						<p>email@example.com</p>
					</Col>
				</Row>
				<Row>
					<Col lg="6" sm="6">
						<h6>OUR LOCATION</h6>
						<p>123 Fake Street, Dasmarinas, Cavite</p>
					</Col>
					<Col lg="6" sm="6">
						<h6>WORKING HOURS</h6>
						<p>Mon-Sat 8:00am-5:00pm</p>
					</Col>
				</Row>

			</Col>

		</Row>
	)
}
