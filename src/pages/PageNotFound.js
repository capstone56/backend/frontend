import {Row, Col, Button, Stack} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function PageNotFound(){
	return (
        <Stack gap={2} className="col-md-5 mx-auto">
            <Row>
                <Col className = "text-center py-5">
                <h1 className='mb-3'>404 - Page Not Found</h1>
                <img src="https://media.giphy.com/media/C21GGDOpKT6Z4VuXyn/giphy.gif" alt="page not found" width="100%"/>
                    <Button className='mt-4' variant="success" as={ Link } to='/' >Return to Home Page</Button>
                </Col>
            </Row>
        </Stack>    
	)
} 


