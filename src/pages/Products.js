// import coursesData from '../data/coursesData';
import { Fragment, useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import { Row, Container } from 'react-bootstrap';


export default function Products() {

	const [products, setProducts] = useState([]);

	const {user} = useContext(UserContext);

	//Retrieve the courses from the database upon initial render of the Course component.
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}products/`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setProducts(data.map(product => {
				return (
					<ProductCard  key={product._id} productProp={product} />
				)
			}))

		})
	}, [])	


	// const courses = coursesData.map(course => {
	// 	return (
	// 		<ProductCard  key={course.id} productProp={course} />
	// 	)
	// })

	return (
		(user.isAdmin)
		?
			<Navigate to="/admin"/>
		:
			<Container><Row>
			<Fragment>
				<h1 className="text-center my-3">Products</h1>
				{products}
			</Fragment>
			</Row></Container>
	)
}

// Props
	// is a shorthand for property since components are considered as object in ReactJS.
	// Props is a way to pass data from the parent to child component.
	// it is synonymous to the function parameter.