import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [password2, setPassword2] = useState("");

	const [isActive, setIsActive] = useState(false);

	function registerUser (e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}users/checkEmail`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === true) {

				Swal.fire({
					title: "Duplicate Email Found",
					icon: "error",
					text: "Kindly provide another email to complete registration."
				})
			} else {

				fetch(`${process.env.REACT_APP_API_URL}users/register`, {
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password,
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if(data === true) {

						// Clear input fields
						setEmail("");
						setPassword("");
						setPassword2("");

						Swal.fire({
							title: "Registration Successful",
							icon: "success",
							text: "Welcome to Zuitt!"
						})

						navigate("/login");

					} else {

						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please, try again."
						})
					}

				})
			}
		})

	}


	useEffect(() => {
		if((firstName !== "" && lastName !== "" && email !== "" && password !== "" && password2 !== "") && (password === password2)) {

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstName, lastName, email, password, password2, ])


	return(

		(user.id !== null) ?
			<Navigate to="/home"/>

			:
			<Card bg={'light'} className="col-lg-3 mx-auto my-5">
			<Card.Header>Login</Card.Header>
			<Card.Body>

			<Form onSubmit={(e) => registerUser(e)}>

				  <h1 className="text-center my-3">create an account</h1>

				  <Form.Group className="mb-3" controlId="userFirstName">
			        <Form.Label>First Name</Form.Label>
			        <Form.Control 
			        	type="text"
			        	value={firstName}
			        	onChange={(e) => {setFirstName(e.target.value)}}
			        	placeholder="Enter your first name" />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="userLastName">
			        <Form.Label>Last Name</Form.Label>
			        <Form.Control 
			        	type="text"
			        	value={lastName}
			        	onChange={(e) => {setLastName(e.target.value)}}
			        	placeholder="Enter your last name" />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        	type="email"
			        	value={email}
			        	onChange={(e) => {setEmail(e.target.value)}}
			        	placeholder="Enter email" />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	value={password}
			        	onChange={(e) => {setPassword(e.target.value)}}
			        	placeholder="Enter Your Password" />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password2">
			        <Form.Label>Verify Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	value={password2}
			        	onChange={(e) => {setPassword2(e.target.value)}}
			        	placeholder="Verify Your Password" />
			      </Form.Group>

			      { isActive ?
			      			<Button variant="danger" type="submit" id="submitBtn">
			      		 	 Submit
			      			</Button>
			      			:
			      			<Button variant="danger" type="submit" id="submitBtn" disabled>
			      			  Submit
			      			</Button>
			      }
			     
			    </Form>	
			    </Card.Body>
		</Card>
		

	)
}