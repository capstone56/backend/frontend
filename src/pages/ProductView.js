import { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Button,InputGroup,Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {

	// useContext hook for global state
	const { user } = useContext(UserContext)

	// Allows us to gain access to methods that will allo us to redirect a user to a different page after ordering in a product. 
	const navigate = useNavigate(); //useHistory

	// The "useParams" hook allows us to retrieve the productId passed via URL
	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);
	const totalAmount = useState();
	

	const addQuantity = () =>{
		setQuantity(quantity + 1)
	}

	const subtractQuantity = () =>{
		if(quantity === 1){
			Swal.fire({
				title: 'Quantity' ,
				icon: 'info',
				text: 'The quantity cannot be less than 1'
			});
		} else{
		setQuantity(quantity - 1)
		}
	}




	const order = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}orders`, {
			method: "POST",
			headers: {
				'Content-Type': "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: user.id,
				productId: productId,
				price: price,
				quantity: quantity,
				totalAmount: totalAmount
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(data === true) {
				Swal.fire({
				title: "Successfully ordered!",
				icon: "success",
				text: "You have successfully ordered for this product."
			})

			navigate("/products");

		} else {

				Swal.fire({
				title: "Something went wrong",
				icon: "error",
				text: "Please try again."
			})
		}

	})

};


	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [productId])

	return (

		<Container className="  my-5"  >
			 <Row className='my-auto'>
				 <Col lg="5" xs="12">
					 <span><img
						 src={require('../img/fake-img.png')}
						 width="100%" className='mb-3'
						 alt="D&M AutoParts"
						 />
					</span>
				</Col>
				<Col lg="7" xs="12">
					<h2>{name}</h2>
					<h6>Description:</h6>
					<p>{description}</p>
					<h5>Price: {price}</h5>
					<>
						<div className='d-flex flex-row mb-5' >
							<div className="quantity-contain " >
								<InputGroup  >
									<Button variant='danger' onClick={() => subtractQuantity(productId)}>-</Button>
										<Form.Control id="myForm"
											value={quantity}
											onChange={(e) => setQuantity(e.target.value)
											}
										/>
									<Button variant='danger' onClick={() => addQuantity(productId)} >+</Button>
								</InputGroup>
							</div>
						</div>
						
					</>
					{
						(user.id !== null) ?
							<Button variant="danger" onClick={() => order(productId)}>Order Now</Button>
							:
							<Button variant="danger" as={Link} to="/login">Log in to Order</Button>
					}

				</Col>
			</Row>

		
		</Container>
	)
}
