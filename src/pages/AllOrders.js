import { useContext, useState, useEffect } from "react";
import {Table} from "react-bootstrap";
import {Navigate} from "react-router-dom";
import UserContext from "../UserContext";



export default function AllOrders(){

	// to validate the user role.
	const {user} = useContext(UserContext);

	//Create allProducts State to contain the products from the database.
	const [allOrders, setAllOrders] = useState([]);

	//"fetchData()" wherein we can invoke if their is a certain change with the product.
	const  fetchData = async () =>{
		// Get all courses in the database
		await fetch(`${process.env.REACT_APP_API_URL}orders/allorders`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})

       
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllOrders(data.map(order => {
				return(
					<tr key={order._id}>
						<td>{order.userId}</td>
						<td>{order.productId}</td>
                        <td>{order.price}</td>
						<td>{order.quantity}</td>
						<td>{order.totalAmount}</td>
						</tr>
				)
			}))

		})
	}


	

	// To fetch all products in the first render of the page.
	useEffect(()=>{
	// 	// invoke fetchData() to get all courses.
		fetchData();
        console.log(fetchData);
	}, [])

    

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>All Orders Page</h1>
				{/*A button to add a new course*/}
				
			</div>
			<Table responsive="md"	 className="edge-round" variant="dark" striped bordered hover>
		     <thead>
		       <tr>
		         
		         <th>User ID</th>
                 <th>Product ID</th>
		         <th>Product Price</th>
		         <th>Quantity</th>
		         <th>TotalAmount</th>
		        
		       </tr>
		     </thead>
		     <tbody>
		       { allOrders }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/allOrders" />
	)
}