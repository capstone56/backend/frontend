import { Fragment } from 'react';
import Banner from '../components/Banner';
import Content1 from '../components/Content1';


export default function Home() {

	return (
		<Fragment>
			<Banner/>
			<Content1/>
		</Fragment>
	)
};