import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import { Form, Button, Card } from 'react-bootstrap';

export default function AddProduct() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [name, setName] = useState('');
	const [category, setCategory] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);
    // const [slots, setSlots] = useState(0);

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // This function will be trigger upon clicking the Save button, and will save the new Product in the database
	function addProduct(e) {

		// Prevents page redirection via form submission
	    e.preventDefault();

	    fetch(`${process.env.REACT_APP_API_URL}products/`, {
	    	method: "POST",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
			    name: name,
			    category: category,
			    description: description,
			    price: price,
			    quantity: quantity
			})
	    })
	    .then(res => res.json())
	    .then(data => {
	    	console.log(data);

	    	if(data){
	    		Swal.fire({
	    		    title: "Product succesfully Added",
	    		    icon: "success",
	    		    text: `${name} is now added`
	    		});

	    		navigate("/admin");
	    	}
	    	else{
	    		Swal.fire({
	    		    title: "Error!",
	    		    icon: "error",
	    		    text: `Something went wrong. Please try again later!`
	    		});
	    	}

	    })

	    // Clear input fields
	    setName('');
	    setCategory('');
	    setDescription('');
	    setPrice(0);
	    setQuantity(0);

	}

	// Submit button validation
	useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
        if(name !== "" && description !== "" && price > 0){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [name, description, price]);

    return (
    	user.isAdmin
    	?
			<>
			<Card bg={'light'} className="col-lg-3 mx-auto my-5">
			<Card.Header>Add Product</Card.Header>
			<Card.Body>
		    	
		        <Form onSubmit={(e) => addProduct(e)}>
		        	<Form.Group controlId="name" className="mb-3">
		                <Form.Label>Product Name</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Product Name" 
			                value = {name}
			                onChange={e => setName(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="name" className="mb-3">
		                <Form.Label>Category</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Product Name" 
			                value = {category}
			                onChange={e => setCategory(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="description" className="mb-3">
		                <Form.Label>Product Description</Form.Label>
		                <Form.Control
		                	as="textarea"
		                	rows={3}
			                placeholder="Enter Product Description" 
			                value = {description}
			                onChange={e => setDescription(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="price" className="mb-3">
		                <Form.Label>Product Price</Form.Label>
		                <Form.Control 
			                type="number" 
			                placeholder="Enter Product Price" 
			                value = {price}
			                onChange={e => setPrice(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="quantity" className="mb-3">
		                <Form.Label>Product Quantity</Form.Label>
		                <Form.Control 
			                type="number" 
			                placeholder="Enter Product Quantity" 
			                value = {quantity}
			                onChange={e => setQuantity(e.target.value)}
			                required
		                />
		            </Form.Group>

		           {/* <Form.Group controlId="slots" className="mb-3">
		                <Form.Label>Product Slots</Form.Label>
		                <Form.Control 
			                type="number" 
			                placeholder="Enter Product Slots" 
			                value = {slots}
			                onChange={e => setSlots(e.target.value)}
			                required
		                />
		            </Form.Group>*/}

		            {/* conditionally render submit button based on isActive state */}
	        	    { isActive 
	        	    	? 
	        	    	<Button variant="primary" type="submit" id="submitBtn">
	        	    		Save
	        	    	</Button>
	        	        : 
	        	        <Button variant="danger" type="submit" id="submitBtn" disabled>
	        	        	Save
	        	        </Button>
	        	    }
	        	    	<Button className="m-2" as={Link} to="/admin" variant="success" type="submit" id="submitBtn">
	        	    		Cancel
	        	    	</Button>
		        </Form>
		        </Card.Body>
		</Card>
	    	</>
    	:
    	    <Navigate to="/products" />
	    	
    )

}