import { useState, useEffect } from 'react';
//import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
// components
import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer';
//import Banner from './components/Banner';
// pages
import Home from './pages/Home';
import AdminDashboard from './pages/AdminDashboard';
import AddProduct from './pages/AddProduct';
import EditProduct from './pages/EditProduct';
import AllOrders from './pages/AllOrders';


import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import MyOrders from './pages/MyOrders';
import PageNotFound from './pages/PageNotFound';
import './App.css';
import { UserProvider } from './UserContext';

function App() {
  
  const [user, setUser] = useState({

    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {

      fetch(`${process.env.REACT_APP_API_URL}users/details`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
      .then(res => res.json())
      .then(data => {

        console.log(process.env)

          // user is logged in
          if(typeof data._id !== "undefined") {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
          } else { // user is logged out

            setUser({
              id: null,
              isAdmin: null
            })

          }
      })

  }, []);


  return (

    <UserProvider value={{user, setUser, unsetUser}} >
        <Router>
            <AppNavbar/>
              
              <Routes>
                <Route path="/" element={<Home/>} />
                  <Route path="/admin" element={<AdminDashboard/>} />
                  <Route path="/addproduct" element={<AddProduct/>} />
                  <Route path="/editproduct/:productId" element={<EditProduct/>} />
                  <Route path="/allorders/" element={<AllOrders/>} />

                  <Route path="/products" element={<Products/>} />
                  <Route path="/products/:productId" element={<ProductView/>} />
                  <Route path="/login" element={<Login/>} />
                  <Route path="/logout" element={<Logout/>} />
                  <Route path="/register" element={<Register/>} />
                  <Route path="/myorders" element={<MyOrders/>} />
                  <Route path="/404" element={<PageNotFound/>} />

              </Routes>
            <Footer/>
        </Router>
    </UserProvider>
    
  );
}

export default App;
